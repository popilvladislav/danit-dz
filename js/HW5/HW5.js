/* Екранування - це зміна принципів роботи спеціальних символів.
Зазвичай вони виконують певні функції в коді. при екрануванні символи сприймаються комп'ютером як звичайні текстові символи. */ 

function createNewUser(){
   let firstName = prompt("Type your first name, please");
   let lastName = prompt("Type your last name, please");
   let birthdayInput = prompt("Type your date of birth in format dd.mm.yyyy");
   const newUser = {
     firstName,
     lastName,
     birthdayInput,
     birthdayDate: new Date(birthdayInput.slice(6, 10), (birthdayInput.slice(3, 6) - 1), birthdayInput.slice(0, 2)),
     getLogin(){
       return (this.firstName.substring(0, 1) + this.lastName).toLowerCase();
   },
     getAge(){
       return Math.floor((new Date() - this.birthdayDate)/(1000*3600*24*365));
   },
     getPassword(){
       return (this.firstName.toUpperCase()).substring(0, 1) + (this.lastName).toLowerCase() + (this.birthdayDate.getFullYear()).toString();
     },
};
console.log(newUser.getLogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())
return newUser;
}
let myUser = createNewUser();
console.log(myUser);
