let changeTheme = document.querySelector("#changeTheme");
let countClick = 1;

let styleFile = document.createElement("link");
document.head.appendChild(styleFile);
styleFile.setAttribute("rel", "stylesheet");
if (localStorage.getItem("theme") === "blue") {
  styleFile.setAttribute(
    "href",
    "file:///C:/Development/Danit/danit-dz/js/HW13/styles/HW13.css"
  );
}
if (localStorage.getItem("theme") === "green") {
  styleFile.setAttribute(
    "href",
    "file:///C:/Development/Danit/danit-dz/js/HW13/styles/HW13_alt.css"
  );
}

changeTheme.addEventListener("click", () => {
  let theme = localStorage.getItem("theme");
  if (theme === "blue") {
    localStorage.theme = "green";
    styleFile.href =
      "file:///C:/Development/Danit/danit-dz/js/HW13/styles/HW13_alt.css";
  } else {
    localStorage.theme = "blue";
    styleFile.href =
      "file:///C:/Development/Danit/danit-dz/js/HW13/styles/HW13.css";
  }
});
