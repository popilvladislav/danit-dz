/* 1. Через const оголошується змінна, значення якої буде задано один раз і не змінюватиметься (константа). 
var і let оголошують змінну, значення якої можна буде змінювати скільки завгодно. 
Замість var рекомендується використовувати let, так як let має блочну область видимості 
(змінна може оголошуватися і використовуватися тільки в рамках блоку коду). var оголошує змінну,
 яка видима у межах функції чи усього коду (глобальна змінна). Змінну через Let можна оголошувати тільки раз на відміну від var.
 2. Для більшої строгості коду і зручності використання змінних у рамках блоків варто використовувати let. */ 

 let userName = prompt("Type your name");
 let userAge = +prompt("Type your age");
 while (+(userName) == 0 || !isNaN(userName)) {
    userName = prompt("Please type your name", userName);
 }
while (isNaN(userAge) || userAge <= 0) {
    userAge = +prompt("Please type your real age with numbers", userAge);
}
 if(userAge < 18) {
     alert("You are not allowed to visit this website");
 } else if(userAge > 22){
     alert("Welcome, " + userName);
    } else {
        if(confirm("Are you sure you want to continue?")) {
            alert("Welcome, " + userName);
        } 
     else {
         alert("You are not allowed to visit this website");
        }
 }