let titles = Array.from(document.querySelectorAll('.tabs-title'));
let texts = Array.from(document.querySelectorAll('.text'));
for(text of texts){
    text.style.display = 'none';
}
titles[0].classList.add('active');
let dataId = titles[0].getAttribute('id');
console.log(dataId);
for(text of texts){
    if(text.getAttribute('data-id') == dataId){
        text.style.display = 'block';
    }
}

titles.forEach(function(title){
    function addActiveClass(){
        let previousActive = document.querySelector('.active')
        previousActive.classList.remove('active');
        texts.forEach(function(text){
    text.style.display = 'none';
    if(text.getAttribute('data-id') == title.id){
        text.style.display = 'block';
        title.classList.add('active');
        ;}
    });
    }
    title.addEventListener('click', addActiveClass)
});