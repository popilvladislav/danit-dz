// Document object model - це представлення усього вмісту сторінки у вигляді об'єктів, до яких можна отримати доступ і змінювати їх.
let mass = ["1", "2", ["6", ["gracias", 134], "sea"], "user", 23];

function list(arr, parent = document.body) {
  let ul = document.createElement(`ul`);
  parent.prepend(ul);
  console.log(arr);
  arr.map((index) => {
    let li = document.createElement(`li`);
    ul.append(li);
    if (!Array.isArray(index)) {
      li.innerText = index;
    } else {
      list(index, li);
    }
  });
  setTimeout(() => ul.remove(), 3000);
}
list(mass, div);

let timer = document.createElement(`div`);
document.body.prepend(timer);

document.body.style.backgroundColor = `rgba(119, 103, 184, 0.6)`;
timer.style.fontSize = `30px`;
timer.style.color = `blue`;

setTimeout(() => timer.remove(), 3000);
let counter = 3;
timer.innerText = `List dissapears in ` + counter;

const seconds = setInterval(() => {
  counter -= 1;
  timer.innerText = `List dissapears in ` + counter;
  if (counter === 0) {
    clearInterval(seconds);
  }
}, 1000);
