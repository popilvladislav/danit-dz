let form = document.querySelector(".password-form");
let icon = document.querySelectorAll(".hidden");
let iconShown = document.querySelectorAll(".shown");
let input = document.querySelectorAll(".input");
let confirm = document.querySelector(".btn");
let confAlert = document.querySelector(".conf_alert");

confirm.onclick = function (event) {
  event.preventDefault();
};
confirm.addEventListener("click", () => {
  if (
    input[0].value !== input[1].value ||
    input[0].value === "" ||
    input[1].value === ""
  ) {
    confAlert.style.display = "block";
    input[1].style.marginBottom = "0";
  } else {
    alert("Ласкаво просимо");
  }
});

form.addEventListener("mousedown", function (event) {
  if (event.target.closest(".input")) {
    confAlert.style.display = "none";
    input[1].style.marginBottom = "24px";
  }
});

form.addEventListener("click", function (event) {
  if (event.target.closest(".hidden") || event.target.closest(".shown")) {
    for (let i = 0; i <= 1; i++) {
      if (event.target === icon[i]) {
        input[i].type = "text";
        icon[i].classList.add("inactive");
        iconShown[i].classList.remove("inactive");
      }
      if (event.target === iconShown[i]) {
        input[i].type = "password";
        icon[i].classList.remove("inactive");
        iconShown[i].classList.add("inactive");
      }
    }
  }
});
