let changeTheme = document.querySelector("#changeTheme");

let styleFile = document.createElement("link");

document.head.appendChild(styleFile);
styleFile.setAttribute("rel", "stylesheet");
if (localStorage.getItem("theme") === "blue") {
  styleFile.setAttribute("href", "./styles/HW13.css");
}
if (localStorage.getItem("theme") === "green") {
  styleFile.setAttribute("href", "./styles/HW13_alt.css");
}

changeTheme.addEventListener("click", () => {
  let theme = localStorage.getItem("theme");
  if (theme === "blue") {
    localStorage.theme = "green";
    styleFile.href = "./styles/HW13_alt.css";
  } else {
    localStorage.theme = "blue";
    styleFile.href = "./styles/HW13.css";
  }
});
