let mines = [1, 14, 22, 26, 29, 30, 40, 60];

let field = document.querySelector(".field");
let fieldCell = Array.from(document.querySelectorAll(".field_cell"));
console.log(fieldCell);

field.addEventListener("click", (event) => {
  if (event.target.closest("button")) {
    let targetIndex = fieldCell.indexOf(event.target);
    event.target.innerHTML = "";
    console.log(targetIndex);
    if (mines.includes(targetIndex)) {
      event.target.classList.add("mine");
      event.target.insertAdjacentHTML(
        "beforeend",
        '<img src="./naval-mine.png" width="24px" height="24px" alt="mine" />'
      );
    } else {
      event.target.classList.add("clicked");
      let counter = 0;

      function checkNearbyCells() {
        //center=========================================
        if (event.target.dataset.id === "center") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 9)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 9)) {
            counter = counter + 1;
          }
        }
        //top_left=========================================
        if (event.target.dataset.id === "top_left") {
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 9)) {
            counter = counter + 1;
          }
        }
        //top==============================================
        if (event.target.dataset.id === "top") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 9)) {
            counter = counter + 1;
          }
        }
        //top_right========================================
        if (event.target.dataset.id === "top_right") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
        }
        //left=============================================
        if (event.target.dataset.id === "left") {
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 9)) {
            counter = counter + 1;
          }
        }
        //right============================================
        if (event.target.dataset.id === "right") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 9)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 8)) {
            counter = counter + 1;
          }
        }
        //bottom_left======================================
        if (event.target.dataset.id === "bottom_left") {
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
        }
        //bottom===========================================
        if (event.target.dataset.id === "bottom") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex + 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 7)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 9)) {
            counter = counter + 1;
          }
        }
        //bottom_right=====================================
        if (event.target.dataset.id === "bottom_right") {
          if (mines.includes(targetIndex - 1)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 8)) {
            counter = counter + 1;
          }
          if (mines.includes(targetIndex - 9)) {
            counter = counter + 1;
          }
        }
        //End of function===============================
      }
      checkNearbyCells();
      event.target.insertAdjacentHTML("beforeend", counter);
    }
  }
});
// Put the flag=====================================
field.addEventListener("contextmenu", (event) => {
  event.preventDefault();
  if (event.target.closest("button")) {
    if (!event.target.classList.contains("flag")) {
      event.target.classList.add("flag");
      event.target.insertAdjacentHTML(
        "beforeend",
        '<img src="./flag.png" width="24px" height="24px" alt="mine" />'
      );
    } else {
      console.log("hfaw");
      event.target.innerHTML = "";
      event.target.classList.remove("flag");
    }
  }
});
