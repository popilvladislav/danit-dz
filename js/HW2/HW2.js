/* Цикли у програмуванні потрібні для створення повторюваних алгоритмів з великим числом ітерацій.
За рахунок циклів можна виконувати такий же блок кода багато разів, 
при цьому запис такої програми є досить компактним. */ 


 let num = +prompt("Please, type the number");
 while (!Number.isInteger(num)) {
    num = +prompt("Please, type the integer number");
 }
 if (num < 5)
 {
    console.log("Sorry, no numbers");
}
 for (let num2 = 1; num2 <= num; num2++) {
     if (num2 % 5 === 0) {
     console.log(num2);
    }
   }