// Для input не рекомендується використовувати події клавіатури, тому що текст можна вставляти через гарячі клавіші, і тоді не відслідкується саме значення у полі.

let buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", function (event) {
  for (item of buttons) {
    item.style.cssText = `background-color: black`;
    if (event.key === item.dataset.id) {
      item.style.cssText = `background-color: blue`;
    }
  }
});
