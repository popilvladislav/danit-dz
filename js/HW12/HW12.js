let images = document.querySelectorAll(".image-to-show");
let startBtn = document.querySelector("#start");
let stopBtn = document.querySelector("#stop");
let timerOut = document.querySelector(".timer");
let time = document.querySelector("#sec");
let miliSec = document.querySelector("#mlSec");
let pause = false;
let second = 2;
let mlsec = 999;
let timeoutSec;
let timeoutMlSec;
//=============================================

let delay = 3000;
let i = 0;
function showImg() {
  switch (i) {
    case 0:
      for (image of images) {
        image.classList.add("image-to-hide");
      }
      images[1].classList.remove("image-to-hide");
      break;
    case 1:
      for (image of images) {
        image.classList.add("image-to-hide");
      }
      images[2].classList.remove("image-to-hide");
      break;
    case 2:
      for (image of images) {
        image.classList.add("image-to-hide");
      }
      images[3].classList.remove("image-to-hide");
      break;
    case 3:
      for (image of images) {
        image.classList.add("image-to-hide");
      }
      images[0].classList.remove("image-to-hide");
      break;
  }
}
//================================================
function indexRot() {
  i++;
  if (i == 4) {
    i = 0;
  }
  showImg();
}
//================================================

function timerSec() {
  let sec = time.innerText;
  sec--;
  if (sec === -1) sec = 2;
  time.innerText = sec;
  timeoutSec = setTimeout(timerSec, 1000);
}
timeoutSec = setTimeout(timerSec, 1000);

//=============================================

function timerMlSec() {
  let mlsec = miliSec.innerText;
  mlsec = mlsec - 5;
  if (mlsec < 0) {
    mlsec = 999;
  }
  miliSec.innerText = mlsec;

  timeoutMlSec = setTimeout(timerMlSec, 5);
}
timeoutMlSec = setTimeout(timerMlSec, 5);
//=============================================

let changeImg = setTimeout(function changeImage() {
  if (!pause) {
    indexRot();
    stopBtn.addEventListener("click", () => {
      pause = true;
    });
    startBtn.addEventListener("click", () => {
      pause = false;
    });
  }
  changeImg = setTimeout(changeImage, delay);
}, delay);

stopBtn.addEventListener("click", () => {
  pause = true;
  clearTimeout(timeoutSec);
  clearTimeout(timeoutMlSec);
  time.innerText = second;
  miliSec.innerText = mlsec;
});
startBtn.addEventListener("click", () => {
  pause = false;
  clearTimeout(timeoutSec);
  clearTimeout(timeoutMlSec);
  timeoutMlSec = setTimeout(timerMlSec, 5);
  timeoutSec = setTimeout(timerSec, 1000);
});
