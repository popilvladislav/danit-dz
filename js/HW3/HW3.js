/* 1. Описать своими словами для чего вообще нужны функции в программировании.
Застосування функцій дозволяє, написавши блок коду, використовувати його за потреби 
у будь-якій частині коду. При необхідності зміни якогось елемента це можна зробити в одному місці,
у функції.
2. Описать своими словами, зачем в функцию передавать аргумент.
 Функція з параметрами універсальна для будь-яких значень аргументів, які туди можна підставити. 
 Так як вхідні дані можуть змінюватися, актуальні аргументи підставляються у функцію і 
 отримуємо потрібний результат.
 */ 
"use strict";

 let num1 = +prompt("Please, type the first number");

 while(isNaN(num1) || num1 == '') {
   num1 = +prompt("You entered Nan. Please, type the first number again", num1);
 }

 let num2 = +prompt("Please, type the second number");

 while(isNaN(num2) || num2 == '') {
   num2 = +prompt("You entered Nan. Please, type the second number again", num2);
 }

 let operation = prompt("Please, enter an operation sign");
 while(operation == '' || !(operation == "+" || operation == "-" || operation == "*" || operation == "/")){
   operation = prompt("Please, enter an existing operation sign among + , -, * , / .", operation);
 }; 
 
 function op() {
    switch(operation) {
      case '+':
         return num1 + num2;
      case '-':
         return num1 - num2;
      case '*':
         return num1 * num2;
      case '/':
         return num1 / num2;   
   }
}
   op();
   console.log('результатом операції '+operation+ ' з числами '+num1+' і '+ num2 +' є '+ op());
