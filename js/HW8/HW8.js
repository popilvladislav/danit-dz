
let warning = document.createElement('p');
document.body.append(warning);
warning.innerText = 'Please enter correct price';
warning.style.display = 'none';

let input = document.createElement('input');
document.body.prepend(input);
input.style.display = 'inline';
input.setAttribute('type', 'number');
input.insertAdjacentText('afterend', '  Price');
input.classList.add('input_price');

function validate(){
    let price = input.value;

    let currPriceMessage = document.createElement('div');
    document.body.prepend(currPriceMessage);
    if(price >= 0 && price != ''){
    currPriceMessage.innerHTML = '<span class="price_message">Текущая цена: $' + price + ' </span><button class="close">X</button>'
let button = document.querySelector('.close');
input.style.color = 'green';
input.style.borderColor = 'black';


function deleteMessage (){
    currPriceMessage.style.display = 'none'
    input.value = '';
    input.style.color = 'black';
     }
button.addEventListener('click', deleteMessage);
    } 
    else {
        if(price != ''){
        warning.style.display = 'block';
        input.style.borderColor = 'red';
        } else {input.style.borderColor = 'black';
      }
    } 
}
function removeWarning(){
    warning.style.display = 'none';
    input.style.borderColor = 'green';
}

input.addEventListener('focusout', validate);
input.addEventListener('focus', removeWarning);
