import gulp from "gulp";
import clean from "gulp-clean";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import autoprefixer from "gulp-autoprefixer";
import cleanCSS from "gulp-clean-css";
import concat from "gulp-concat";
import minifyjs from "gulp-uglify";
import rename from "gulp-rename";
import browserSync from "browser-sync";
import imagemin from "gulp-imagemin";
const sass = gulpSass(dartSass);

export const cleanDist = () => {
  return gulp.src("./dist/*", { read: false }).pipe(clean());
};

export const styleCompile = () => {
  return gulp
    .src("./src/styles/*.scss")
    .pipe(sass())
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"],
        cascade: false,
      })
    )
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(rename("styles.min.css"))
    .pipe(gulp.dest("./dist/"));
};

export const scripts = () => {
  return gulp
    .src("./src/scripts/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest("./dist/"));
};

export const imageOpt = () => {
  return gulp
    .src("./src/img/**/*")
    .pipe(imagemin())
    .pipe(gulp.dest("./dist/img"));
};
//Task Build
export const build = gulp.series(
  cleanDist,
  gulp.parallel(styleCompile, scripts, imageOpt)
);

//Task Dev
const liveServer = browserSync.create();
export const dev = gulp.series(build, () => {
  liveServer.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch(
    "./src/**/*",
    gulp.series(build, (done) => {
      liveServer.reload();
      done();
    })
  );
  gulp.watch("./index.html", (done) => {
    liveServer.reload();
    done();
  });
});
