const menuOpen = document.querySelector(".header__menu-open-btn");
const menuClose = document.querySelector(".header__menu-close-btn");
const menu = document.querySelector(".drop-menu__block");

menuOpen.addEventListener("click", () => {
  menuOpen.classList.add("inactive");
  menuClose.classList.remove("inactive");
  menu.classList.remove("inactive");
});

function closeMenu() {
  menuOpen.classList.remove("inactive");
  menuClose.classList.add("inactive");
  menu.classList.add("inactive");
}

menuClose.addEventListener("click", () => {
  closeMenu();
});
