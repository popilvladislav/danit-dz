// try...catch конструкцію доцільно використовувати у ситуаціях, коли потрібно відловити можливу помилку, опрацювати її потрібним чином, і продовжити виконання програми.
// вона може використовуватися, якщо є недовіра до third party data.
"use strict";

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const listContainer = document.querySelector("#root");
const outerList = document.createElement("ul");
listContainer.append(outerList);

class MissingAuthorError extends Error {
  constructor(obj) {
    super();
    this.obj = obj;
    this.bookName = obj.name;
    this.name = "MissingAuthorError";
    this.message = `the ${this.bookName} has no AUTHOR record`;
  }
}

class MissingPriceError extends Error {
  constructor(obj) {
    super();
    this.obj = obj;
    this.bookName = obj.name;
    this.name = "MissingPriceError";
    this.message = `the ${this.bookName} has no PRICE record`;
  }
}

class Book {
  constructor(obj) {
    if (!Object.hasOwn(obj, "author")) {
      throw new MissingAuthorError(obj);
    } else if (!Object.hasOwn(obj, "price")) {
      throw new MissingPriceError(obj);
    }
    this.obj = obj;
  }
  render() {
    const outerLi = document.createElement("li");
    const innerList = document.createElement("ul");
    for (let i = 0; i <= 2; i++) {
      let innerLi = document.createElement("li");
      innerList.append(innerLi);
      innerLi.innerText = `${Object.keys(this.obj)[i]}:${
        Object.values(this.obj)[i]
      }`;
    }
    outerLi.append(innerList);
    outerList.append(outerLi);
  }
}

books.forEach((elem) => {
  try {
    new Book(elem).render();
  } catch (err) {
    if (err.name === "MissingAuthorError" || err.name === "MissingPriceError") {
      console.error(err);
    } else {
      throw err;
    }
  }
});
