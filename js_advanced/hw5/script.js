const container = document.querySelector(".container");
const loader = document.querySelector(".loader");

class Post {
  constructor(postId, title, text, userName, userEmail) {
    this.postId = postId;
    this.title = title;
    this.text = text;
    this.userName = userName;
    this.userEmail = userEmail;

    this.post = document.createElement("div");
    this.postName = document.createElement("p");
    this.btnDelete = document.createElement("button");
    this.postEmail = document.createElement("a");
    this.postTitle = document.createElement("p");
    this.postText = document.createElement("p");
  }

  render() {
    this.postEmail.setAttribute("href", `mailto:${this.userEmail}`);
    const header = document.createElement("div");
    header.className = "post-header";
    header.append(this.postName, this.btnDelete);
    this.post.append(header);

    this.postName.innerText = this.userName;
    this.postEmail.innerText = this.userEmail;
    this.postTitle.innerText = this.title;
    this.postText.innerText = this.text;
    this.btnDelete.innerText = "delete";

    this.post.className = "post";
    this.postName.className = "post-header--name";
    this.btnDelete.className = "post-btnDelete";
    this.postEmail.className = "post-email";
    this.postTitle.className = "post-title";
    this.postText.className = "post-text";

    this.post.append(this.postEmail, this.postTitle, this.postText);
    container.append(this.post);

    this.btnDelete.addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`).then(
        ({ status }) => {
          if (status === 200) {
            this.post.remove();
            console.log(status);
          }
        }
      );
    });
  }
}

fetch("https://ajax.test-danit.com/api/json/users")
  .then((res) => res.json())
  .then((users) => {
    users.forEach(({ id, name, email }) => {
      fetch("https://ajax.test-danit.com/api/json/posts")
        .then((res) => res.json())
        .then((posts) => {
          posts.forEach(({ id: postId, userId, title, body }) => {
            if (id === userId) {
              new Post(postId, title, body, name, email).render();
              loader.remove();
            }
          });
        });
    });
  });
