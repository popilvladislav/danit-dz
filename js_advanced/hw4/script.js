const container = document.querySelector(".container");

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((res) => res.json())
  .then((data) => {
    console.log(data);
    data.forEach(({ episodeId, name, openingCrawl, characters }) => {
      const filmCard = document.createElement("div");
      const filmName = document.createElement("p");
      const actorList = document.createElement("ul");
      const filmId = document.createElement("p");
      const filmOpeningCrawl = document.createElement("p");

      filmCard.classList.add("film-card");
      filmName.classList.add("film-name");
      actorList.classList.add("film-actorList");
      filmId.classList.add("film-id");
      filmOpeningCrawl.classList.add("film-openingCrawl");

      filmName.innerText = name;
      filmId.innerText = `Episode: ${episodeId}`;
      filmOpeningCrawl.innerText = openingCrawl;
      //   actorList.innerHTML = `<span class="spinner"></span>`;

      container.append(filmCard);
      filmCard.append(filmName, actorList, filmId, filmOpeningCrawl);
      characters.forEach((url) => {
        fetch(url)
          .then((res) => res.json())
          .then(({ name: actorName, url: actorUrl }) => {
            actorList.insertAdjacentHTML(
              "beforeend",
              `<li><a href=${actorUrl} class="actor-name">${actorName}</a></li>`
            );
          });
      });
    });
  });
