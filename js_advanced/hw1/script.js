/*1. Прототипне наслідування довзволяє оптимізувати роботу програми. 
Це дає можливість створювати нові обєкти на основі базових (прототипів), без копіювання методів. 
це основний принцип побудови ОО-програм.
2. super() викликається у конструкторі класу-нащадка, щоб отримати функцію-конструктор класу-предка.
*/

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    if (value.length < 3) {
      alert("Very short name");
      return;
    } else {
      this._name = value;
    }
  }
  get age() {
    return this._age;
  }
  set age(value) {
    if (value >= 18 && value <= 65) {
      this._age = value;
    } else {
      alert("not appropriate age range");
      return;
    }
  }
  get salary() {
    return this._salary;
  }

  set salary(value) {
    if (value > 0) this._salary = value;
    else alert("Salary cannot be less than 1");
  }
}

class Programmer extends Employee {
  constructor(lang, ...args) {
    super(...args);
    this.lang = [...lang];
  }
  get salary() {
    return this._salary * 3;
  }
}

const Vlad = new Programmer(["js", "html", "css"], "Vlad", 28, "1850");
console.log(Vlad.salary);
const George = new Programmer(["js", "C++", "php"], "George", 34, "4300");
const Mykola = new Programmer("html", "Mykola", 24, "700");
console.log(Vlad, George, Mykola);
