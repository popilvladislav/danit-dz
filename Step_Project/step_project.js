//Our services block
let titles = Array.from(document.querySelectorAll(".service_item"));
let texts = Array.from(document.querySelectorAll(".service_content"));

function addActiveClass(title) {
  title.classList.add("active");
}

function removeActiveClass() {
  for (item of titles) {
    item.classList.remove("active");
  }
}

function displayText(text) {
  text.style.cssText = `display: grid;
grid-template-columns: 192px 1fr;
grid-template-rows: 1;
grid-column-gap: 15px;
margin-bottom: 90px;`;
}

function notDisplayText() {
  for (text of texts) {
    text.style.display = "none";
  }
}

function searchText(title) {
  for (text of texts) {
    if (title.id == text.dataset.id) {
      displayText(text);
    }
  }
}
addActiveClass(titles[0]);
searchText(titles[0]);

for (let i = 0; i <= 5; i++) {
  titles[i].addEventListener("click", () => {
    removeActiveClass();
    notDisplayText();
    addActiveClass(titles[i]);
    searchText(titles[i]);
  });
}
//========================================================
//Our Amazing Work

let workFilter = document.querySelector(".work_filter");
let filteringButtons = document.querySelectorAll(".work_type");
let additionalImg = document.querySelectorAll(".add_photo");
let additionalImgSecond = document.querySelectorAll(".add_photo_sec");
let loadMoreButton = document.querySelectorAll(".load_more");
let images = document.getElementsByClassName("work_item");
let workList = document.getElementById("workListGrid");
let crossAnimated = document.querySelectorAll(".cross");

workFilter.addEventListener("click", (event) => {
  if (event.target.closest("button") && event.target !== workFilter) {
    for (button of filteringButtons) {
      button.classList.remove("work_type_active");
    }
    event.target.classList.add("work_type_active");

    images = document.getElementsByClassName("work_item");

    for (item of images) {
      item.style.display = "block";
      if (item.dataset.id !== event.target.id) {
        item.style.display = "none";
      }
      if (event.target.id == "all") {
        item.style.display = "block";
      }
    }
  }
});

let clickCounter = 0;

loadMoreButton[0].addEventListener("click", () => {
  crossAnimated[0].style.animationPlayState = "running";
  setTimeout(() => {
    clickCounter += 1;
    let activeId = document.getElementsByClassName("work_type_active")[0].id;
    let sectionAmaz = document.querySelector(".amazing_work");

    function displayAddPhoto() {
      img.classList.add("work_item");

      if (img.dataset.id == activeId) {
        img.style.display = "block";
      }
      if (activeId == "all") {
        img.style.display = "block";
      }
    }
    for (img of additionalImg) {
      displayAddPhoto();
    }
    if (clickCounter == 2) {
      loadMoreButton[0].style.display = "none";
      sectionAmaz.style.padding = "96px 0 20px";
      for (img of additionalImgSecond) {
        displayAddPhoto();
      }
    }
    crossAnimated[0].style.animationPlayState = "paused";
  }, 2000);
});
//============================================================================
//What People Say About theHam
let scrollBlock = document.querySelector("#scrollButtons");
let quoteBlocks = document.querySelectorAll(".quote_block");
//buttons
let scrollButtons = document.querySelectorAll(".quote_scroll_button");
let scrollImage = document.querySelectorAll(".quote_scroll_image");
let arrLeft = document.querySelector("#left");
let arrRight = document.querySelector("#right");

function makeActive() {
  let activeAuthorId = document.querySelector(".button_active").id;
  for (item of quoteBlocks) {
    item.classList.remove("block_active");
    if (item.dataset.id == activeAuthorId) {
      item.classList.add("block_active");
    }
  }
}

for (let i = 0; i <= 3; i++) {
  scrollButtons[i].addEventListener("click", () => {
    for (button of scrollButtons) {
      button.classList.remove("button_active");
    }
    //=====================
    for (image of scrollImage) {
      image.classList.remove("image_active");
    }
    //=====================
    scrollButtons[i].classList.add("button_active");
    //==========================================
    scrollImage[i].classList.add("image_active");
    makeActive();
  });
}

arrLeft.addEventListener("click", () => {
  let activeAuthor = document.querySelector(".button_active");

  for (button of scrollButtons) {
    if (activeAuthor == button) {
      let indexActive = Array.from(scrollButtons).indexOf(button);

      if (indexActive != 0) {
        button.classList.remove("button_active");
        //=================
        for (image of scrollImage) {
          image.classList.remove("image_active");
        }
        //=================
        let newIndex = indexActive - 1;
        scrollButtons[newIndex].classList.add("button_active");
        scrollImage[newIndex].classList.add("image_active");
      }
    }
  }
  makeActive();
});

arrRight.addEventListener("click", () => {
  let activeAuthor = document.querySelector(".button_active");

  for (button of scrollButtons) {
    if (activeAuthor == button) {
      let indexActive = Array.from(scrollButtons).indexOf(button);

      if (indexActive != scrollButtons.length - 1) {
        button.classList.remove("button_active");
        //======================
        for (image of scrollImage) {
          image.classList.remove("image_active");
        }
        //======================
        let newIndex = indexActive + 1;
        scrollButtons[newIndex].classList.add("button_active");
        scrollImage[newIndex].classList.add("image_active");
      }
    }
  }
  makeActive();
});

// Gallery
const msnry = new Masonry(".gallery", {
  columnWidth: 373,
  gutter: 20,
  itemSelector: ".gallery_item",
});

// Gallery add more images====================

let galleryAdd = document.querySelectorAll(".hide_img");

loadMoreButton[1].addEventListener("click", () => {
  crossAnimated[1].style.animationPlayState = "running";
  setTimeout(() => {
    for (img of galleryAdd) {
      img.classList.remove("hide_img");
    }
    crossAnimated[1].style.animationPlayState = "paused";
    loadMoreButton[1].style.display = "none";
    const msn = new Masonry(".gallery", {
      columnWidth: 373,
      gutter: 20,
      itemSelector: ".gallery_item",
    });
  }, 2000);
});
